# Skiff Desktop Linux

# Why?

Was bored so I did this very simple Linux Electron based client which opens Skiff Pages by default, as that's what I use most. Feel free to change this as you please and need.



# Install

If you want to install this App, just clone this repo to a destination of your choice, open a terminal & type "npm install". This does not require root privileges or anything of the sort.



# Things to keep in mind

This is a project based on a 2 sentence prompt sent to ChatGPT-4. It does not contain a X11 App Icon, or a .desktop launcher. Launching it as of right now is completely done via the terminal with "npm start" inside the directory you've cloned the repo to.



# Contribute

Idk what to say here. Pull requests are welcome I guess. Not too experienced with Linux App development or deployment, just wanted this simple App to work with.
